defmodule Stack do
  defstruct elements: []

  def new, do: %Stack{}

  def push(stack, element) do
    # Update the stack, add the new element to the head of the List
    # of elements
    %Stack{stack | elements: [element | stack.elements]}
  end

  def pop(%Stack{elements[]}), do: raise("Empty stack!")
  def pop(%Stack{elements: [top | rest]}), do: { top: %Stack{elements: rest} }
end
