defmodule TH do
  defp mv(from, to), do: IO.puts "Moving from #{from} to #{to}"

  def th(1, src, dest, _), do: mv(src, dest)

  @doc """
  Tower of Hanoi recursive solution
  n is the total number of disks, where 1 is the smallest(top) and n is the largest(bottom)

  Call TH.th(3, 1, 3, 2)
  """
  def th(n, src, dest, aux) do
    # Move n - 1 disks from src (peg A) to aux (peg B)
    th(n - 1, src, aux, dest)
    # Move from peg A to peg C
    mv(src, dest)
    # Move from peg B(aux) to peg C(dest)
    th(n - 1, aux, dest, src)
  end
end
