# FizzBuzz

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `fizz_buzz` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:fizz_buzz, "~> 0.0.1"}]
    end
    ```

  2. Ensure `fizz_buzz` is started before your application:

    ```elixir
    def application do
      [applications: [:fizz_buzz]]
    end
    ```

