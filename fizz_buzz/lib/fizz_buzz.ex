defmodule FizzBuzz do
  def calculate(0, 0, _), do: "FB"

  def calculate(_, 0, _), do: "B"

  def calculate(0, _, _), do: "F"

  def calculate(_, _, num), do: num

  def fizz_buzz([first_denom, second_denom, num]) do
    1..num |> Enum.map(fn(x) -> calculate(rem(x, first_denom), rem(x, second_denom), x) end)
    |> Enum.join(" ")
  end

  def main(args) do
    for line <- args |> File.stream! do
      arr = String.strip(line)
      |> String.split(" ")
      arr = Enum.map(arr, fn(x) -> Integer.parse(x) end)
      |> Enum.map(&elem(&1, 0))
      
      fizz_buzz(arr) |> IO.puts
    end
  end
end
