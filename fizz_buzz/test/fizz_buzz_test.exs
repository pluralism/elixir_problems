defmodule FizzBuzzTest do
  use ExUnit.Case
  doctest FizzBuzz

  test "it returns the correct value for input '3 5 0'" do
    assert "1 2 F 4 B F 7 8 F B" = FizzBuzz.fizz_buzz([3, 5, 10])
  end

  test "it returns the correct value for input '2 7 15'" do
    assert "1 F 3 F 5 F B F 9 F 11 F 13 FB 15" = FizzBuzz.fizz_buzz([2, 7, 15])
  end
end
