# MimeType

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `mime_type` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:mime_type, "~> 0.0.1"}]
    end
    ```

  2. Ensure `mime_type` is started before your application:

    ```elixir
    def application do
      [applications: [:mime_type]]
    end
    ```

