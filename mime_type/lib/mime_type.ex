defmodule MimeType do
  def build_map(map, 0), do: map

  def build_map(map, count) do
    [ext, mime] = IO.gets("") |> String.strip |> String.split
    new_map = Map.put_new(map, String.downcase(ext), mime)
    build_map(new_map, count - 1)
  end

  def evaluate(_, ""), do: IO.puts("UNKNOWN")

  def evaluate(map, s) do
    [_, ext] = String.split(s, ".")
    mime = Map.get(map, ext)
    cond do
      is_nil(mime) || String.length(mime) == 0 -> IO.puts("UNKNOWN")
      String.length(mime) > 0 -> IO.puts(mime)
    end
  end

  def get_exts(map, q) do
    for _ <- 1..q do
      ext = "a" <> IO.gets("") |>
      String.strip |>
      String.downcase |>
      Path.extname
      evaluate(map, ext)
    end
  end

  def main do
    n = IO.gets("") |> Integer.parse |> elem(0)
    q = IO.gets("") |> Integer.parse |> elem(0)
    get_exts(build_map(%{}, n), q)
  end
end
