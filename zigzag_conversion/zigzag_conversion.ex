defmodule ZigzagConversion do
  @moduledoc """
    My solution to https://leetcode.com/problems/zigzag-conversion/ challenge
  """

  @doc """
    Build a map in the form %{1 => [], 2 => {}}, where each key represents
    the index of an arbitrary array
    Each index of this map represents a row in a multidimensional array
  """
  def build_map(map, 0), do: Map.put_new(map, 0, [])

  def build_map(map, count) do
    build_map(Map.put_new(map, count, []), count - 1)
  end

  def put_in_index(map, index, value) do
    Map.update(map, index, [], &([value | &1]))
  end

  @doc """
    Converts the string in the Zigzag form
    It does it by iterating over the string and adding each character to the
    correct arbitrary row
  """
  def convert(s, _, _, _, num_rows) when num_rows <= 1, do: s

  def convert("", _, current_map, _, _) do
    Enum.map(current_map, fn {_, v} -> [Enum.reverse(v)] end) |> List.flatten
  end

  def convert(<<char::utf8, rest::bits>>, dir, current_map, current_row, num_rows) do
    new_map = put_in_index(current_map, current_row, [char])
    val = cond do
      (dir == 1 && current_row == num_rows - 1) || (dir == -1 && current_row == 0) -> dir * -1
      :otherwise -> dir
    end
    convert(rest, val, new_map, current_row + (1 * val), num_rows)
  end

  def main(s, num_rows) do
    convert(s, 1, build_map(%{}, num_rows - 1), 0, num_rows)
  end
end
